function openTab(evt, tabID, tabName, parentLinksClass) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName(tabName);
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        if ( tablinks[i].parentElement.className === parentLinksClass )
         tablinks[i].className = tablinks[i].className.replace(" active", "");

    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(tabID).style.display = "block";
    evt.currentTarget.className += " active";
}


function setCircleProgress(id) {
    let circle = document.getElementById(id);

    if (!circle) {
        return
    }

    let radius = circle.r.baseVal.value;
    let circumference = radius * 2 * Math.PI;

    circle.style.strokeDasharray = `${circumference} ${circumference}`;
    circle.style.strokeDashoffset = `${circumference}`;

    const offset = circumference - circle.getAttribute('val') / 100 * circumference;
    circle.style.strokeDashoffset = offset;
}

setCircleProgress('circleprogress1');
setCircleProgress('circleprogress2');
setCircleProgress('circleprogress3');
setCircleProgress('circleprogress4');



/* Mobile nav
    ==================*/

const navToggle = $("#navToggle");
const nav = $("#nav");

navToggle.on("click", function(event) {
    event.preventDefault();

    nav.toggleClass("show");
});



//
$('.breadcrumbs li a').each(function(){

    var breadWidth = $(this).width();

    if($(this).parent('li').hasClass('active') || $(this).parent('li').hasClass('first')){



    } else {

        $(this).css('width', 75 + 'px');

        $(this).mouseover(function(){
            $(this).css('width', breadWidth + 'px');
        });

        $(this).mouseout(function(){
            $(this).css('width', 75 + 'px');
        });
    }


});
